﻿#include <iostream>
using namespace std;
#include <time.h>

int main()
{
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);

    int date = buf.tm_mday;
    const int N = 5;
    int array[N][N];

    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            array[i][j] = i + j;
        }
    }

    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            cout << array[i][j] << " ";
        }
        cout << endl;    
    }

    int sum = 0;
    int row = date % N;
    for (int j = 0; j < N; j++) {
        sum += array[row][j];
    }
    cout << "sum " << row << ": " << sum << endl;

    return 0;
}


